import { Controller, Get, Param } from '@nestjs/common';
import { RestaurantsService } from './restaurants.service';
import { Restaurant } from 'src/models/Restaurant';
import { Restaurants } from 'src/models/Restaurants';

@Controller('restaurants')
export class RestaurantsController {
    constructor(private readonly restaurantService: RestaurantsService) {}

    @Get()
    async index(): Promise<Restaurants> {
        return this.restaurantService.getRestaurants();
    }

    @Get(':id')
    async find(@Param('id') id: number): Promise<Restaurant> {
        return this.restaurantService.getRestuarant(id);
    }
}
