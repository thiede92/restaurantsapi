import { Injectable } from '@nestjs/common';
import { Restaurant } from 'src/models/Restaurant';

@Injectable()
export class RestaurantsService {
    private readonly restaurantList: Restaurant[] = [
        {
            id: 0,
            name: "Mensa Akademicka WSG",
            address: {
                street: "Garbary",
                number: "2",
                zipCode: "85-229",
                city: "Bydgoszcz"
            },
            phone: "722097496",
            email: "nadamczyk88@interia.pl",
            website: "https://mensa.byd.pl",
            imgName: "mensa-wsg.jpg"
        },
        {
            id: 1,
            name: "CK Chicken Kebab",
            address: {
                street: "Ogrody",
                number: "27a",
                zipCode: "85-870",
                city: "Bydgoszcz"
            },
            phone: "516166001",
            email: "kebabchickenbydgoszcz@gmail.com",
            website: "https://kebab-chicken.pl/",
            imgName: "chicken-kebab.png"
        },
        {
            id: 2,
            name: "Pizza Mario Wyżyny",
            address: {
                street: "Magnuszewska ",
                number: "3",
                zipCode: "85-861",
                city: "Bydgoszcz"
            },
            phone: "+48 52 582 18 88",
            email: "pizzamario@gmail.com",
            website: "https://www.pizzamario.com/",
            imgName: "pizzamario.jpg"
        },
        {
            id: 3,
            name: "Pasibus Bydgoszcz",
            address: {
                street: "Stary Rynek",
                number: "21",
                zipCode: "85-105",
                city: "Bydgoszcz"
            },
            phone: "739 907 689",
            email: "czesc@pasibus.pl",
            website: "https://www.pasidostawa.pl",
            imgName: "pasibus.jpg"
        }
    ]

    getRestaurants(): Restaurant[] {
        return this.restaurantList;
    }

    getRestuarant(id: number): Restaurant {
        const index = this.restaurantList.findIndex(obj => obj.id == id);
        return this.restaurantList[index];
    }
}
