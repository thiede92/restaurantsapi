export class Restaurant {
    readonly id: number;
    readonly name: string;
    readonly address: {
        street: string;
        number: string;
        zipCode: string;
        city: string;
    };
    readonly phone: string;
    readonly email: string;
    readonly website: string;
    readonly imgName: string;
    readonly reviews?: [];
}