import { Restaurant } from './Restaurant';

export class Restaurants {
    [key: number]: Restaurant
}